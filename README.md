# Api/casoft Init Jour : Git

## Description

Repo de la formation git de l'api/casoft Init. **En cas de questions :** [remy.huet@etu.utc.fr](mailto://remy.huet@etu.utc.fr), [thibaud@duhautbout.ovh](mailto://thibaud@duhautbout.ovh), [quentinduchemin@tuta.io](mailto://quentinduchemin@tuta.io).

Pour aller plus loin : lire la [documentation de git](https://git-scm.com/docs).

En cas de remarques sur la présentation (ou de questions aussi), vous pouvez utiliser le [système d'issues](https://gitlab.utc.fr/picasoft/apis/h19/init/git/issues).

## Chaîne d'intégration

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Après construction (uniquement sur la branche `master`), le document final est disponible [à cette adresse](https://uploads.picasoft.net/api/git.pdf).

Le chemin d'upload est défini dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).

## Contribuer !

Créez une branche, et faites une merge request ! Faites la relire par un tiers qui la valide et la merge.
N'oubliez pas d'ajouter votre nom dans la license et dans les auteurs de `main.tex`

## Plan

Le plan est disponible [ici](docs/plan.md)

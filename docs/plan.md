# Plan de la présentation

## À propos

La formation sur git durera une journée (~6/7 heures). Elle abordera à la fois l'utilisation de Git (local et remotes) mais aussi les méthodes de gestion de travail propres à GitLab.

**Comment lire ce plan ?**
Les sections sont numérotés en niveau 1. Les sous-sections en niveau 2. Une indentation sous la section/sous section est un titre, deux un sous-titre. Un niveau sans "fils" est une frame.

## Plan

1. Introduction

    * OK - Qu'est ce que git ?
    * OK - Pourquoi la gestion de version ?
    * OK - Différents logiciels de gestion de version
    * OK - Petite histoire de Git
    *(transition ?)*

2. Versionner son travail

    1. Configuration et initialisation

        * OK - Création d'un dépôt Git
		* OK - Configurer son identité
        * OK - Configuration locale / configuration globale
    2. Gestion théorique
        * OK - Fonctionnement de Git
        * Working Directory vs. Staging Area vs. Repository
            * OK - Explications
    3. Gestion linéaire en pratique
        * Créer des versions
            * OK - Working directory <-> staging area
			* OK - Exemples
            * OK - Staging area <-> repository
        * OK - Dissection d'un commit
        * Visualiser les différences
            * OK - Git log
            * OK - Git diff
            * OK - Git show

3. Utiliser les versions
    1. Le HEAD
    	* Qu'est-ce que le HEAD ?
    2. Marquer une version
        * Git tag
    3. Mettre de côté ses modifications
        * Enregistrer les modifications locales
        * Restaurer les modifications locales
    4. Parcourir l'historique
        * Changer de version
        * Revenir au dernier commmit
        * Annuler des changements
    5. Annuler des commits
        * Sans modification d'historique
        * Avec modification d'historique
            * Git reset
            * Git commit --amend

4. Utilisation des remotes
    1. Présentation
        * Le concept des remotes
        * Exemples de remotes
        * Création d'un dépôt distant
    2. Récupérer du travail existant
        * Cloner un dépôt existant
        * Tirer des changements
    3. Envoyer son travail
        * Pousser des commits

5. Gestion non linéaire [branche gestion_non_lineaire non mergée]
    1. Explications théoriques
        * OK - Principe de gestion non linéaire
        * Création d'une divergence
			* OK - Principe de la gestion non linéaire
            * OK - Explications
            * OK - Création d'une divergence : analyse et mise en contexte 
            * OK - Illustrations / Exemple
        * OK - Fusion
    2. Application à Git
        * OK - Gestion des branches
        * OK - Changer de branche
        * OK - Visualisation
    3. Fusionner des branches
        * OK - Le merge
        * Le rebase
            * OK - Avertissement
            * OK - Comparaison avec le merge
            * OK - Application simple
            * Rebase interactif
        * Le cherry-pick

6. Résolution de conflits
    1. Les conflits
        * Qu'est-ce qu'un conflit ?
        * Exemple de situation conflictuelle
        * Comment se présente un conflit ?
        * Concrètement, quand peut-on avoir un conflit ?
            * Merge
            * Rebase
            * Pull
    2. Résoudre un conflit
        * En théorie
        * Application
    3. Historiques divergents
        * Quand parle-t-on de divergence ?
        * Comment diverge-t-on ?
        * Résoudre des divergences ?

7. Travail collaboratif avec Git et GitLab
    1. Présentation de git flow
    2. Présentation de GitLab
